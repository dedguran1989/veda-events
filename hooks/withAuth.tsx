// components/withAuth.js
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { getUserFromCookie } from '../lib/utils' // You need to implement this function

const withAuth = (WrappedComponent, allowedRoles) => {
  return (props) => {
    const router = useRouter()
    useEffect(() => {
      const user = getUserFromCookie()
      if (!user || !allowedRoles.includes(user.role)) {
        router.push('/login')
      }
    }, [])

    return <WrappedComponent {...props} />
  }
}

export default withAuth
