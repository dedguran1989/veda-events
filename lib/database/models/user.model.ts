import { Schema, model, models } from 'mongoose'

export interface IUser {
  _id: string
  email: string
  username: string
  photo?: string
  password: string
  role: 'admin' | 'user'
  createdAt: Date
  updatedAt: Date
}
export const UserSchema = new Schema({
  email: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  photo: { type: String, required: false },
  password: { type: String, required: true },
  role: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
})

const User = models?.User || model<IUser>('User', UserSchema)

export default User
