import { Schema, model, models } from 'mongoose'

export interface IFavoriteEvent {
  _id: string
  userId: string
  eventId: string
}

const FavoriteEventSchema = new Schema({
  userId: { type: String, required: true },
  eventId: { type: String, required: true },
})

const FavoriteEvent =
  models?.FavoriteEvent ||
  model<IFavoriteEvent>('FavoriteEvent', FavoriteEventSchema)

export default FavoriteEvent
