import { type ClassValue, clsx } from 'clsx'

import { twMerge } from 'tailwind-merge'
import qs from 'query-string'

import { UrlQueryParams, RemoveUrlQueryParams, Event } from '@/types'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export const formatDateTime = (dateString: Date) => {
  const dateTimeOptions: Intl.DateTimeFormatOptions = {
    weekday: 'short', // abbreviated weekday name (e.g., 'Mon')
    month: 'short', // abbreviated month name (e.g., 'Oct')
    day: 'numeric', // numeric day of the month (e.g., '25')
    hour: 'numeric', // numeric hour (e.g., '8')
    minute: 'numeric', // numeric minute (e.g., '30')
    hour12: true, // use 12-hour clock (true) or 24-hour clock (false)
  }

  const dateOptions: Intl.DateTimeFormatOptions = {
    weekday: 'short', // abbreviated weekday name (e.g., 'Mon')
    month: 'short', // abbreviated month name (e.g., 'Oct')
    year: 'numeric', // numeric year (e.g., '2023')
    day: 'numeric', // numeric day of the month (e.g., '25')
  }

  const timeOptions: Intl.DateTimeFormatOptions = {
    hour: 'numeric', // numeric hour (e.g., '8')
    minute: 'numeric', // numeric minute (e.g., '30')
    hour12: true, // use 12-hour clock (true) or 24-hour clock (false)
  }

  const formattedDateTime: string = new Date(dateString).toLocaleString(
    'ru-RU',
    dateTimeOptions
  )

  const formattedDate: string = new Date(dateString).toLocaleString(
    'ru-RU',
    dateOptions
  )

  const formattedTime: string = new Date(dateString).toLocaleString(
    'ru-RU',
    timeOptions
  )

  return {
    dateTime: formattedDateTime,
    dateOnly: formattedDate,
    timeOnly: formattedTime,
  }
}
export function checkEmail(email: string, eventEmail: string) {
  let eventOrganizerEmail
  if (!email) {
    eventOrganizerEmail = eventEmail
  } else {
    eventOrganizerEmail = email.toString()
  }
  return eventOrganizerEmail
}
export const uploadImageInLocal = async (
  file: File,
  uploadFileInLocal: string | null
) => {
  const formData = new FormData()
  if (uploadFileInLocal) {
    formData.append('uploadImage', uploadFileInLocal)
  }
  formData.append('file', file)

  try {
    const response = await fetch('/api/upload', {
      method: 'POST',
      body: formData,
    })

    if (!response.ok) {
      throw new Error('Загрузка фото не удалась')
    }

    const data = await response.json()
    return data.url
  } catch (error) {
    console.error('Ошибка в загрузке фото:', error)
  }
}
export const deleteLocalFile = async (file: string) => {
  try {
    const response = await fetch(
      `/api/upload?filePath=${encodeURIComponent(file)}`
    )
    const result = await response.json()

    if (!result.success) {
      console.error('Не получилось удалить файл:', result.message)
    }
  } catch (error) {
    console.error('Не получилось удалить файл:', error)
  }
}
export const convertFileToUrl = (file: File) => URL.createObjectURL(file)

export const formatPrice = (price: string) => {
  const amount = parseFloat(price)
  const formattedPrice = new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency: 'RUB',
  }).format(amount)

  return formattedPrice
}

export function formUrlQuery({ params, key, value }: UrlQueryParams) {
  const currentUrl = qs.parse(params)

  currentUrl[key] = value

  return qs.stringifyUrl(
    {
      url: window.location.pathname,
      query: currentUrl,
    },
    { skipNull: true }
  )
}

export function removeKeysFromQuery({
  params,
  keysToRemove,
}: RemoveUrlQueryParams) {
  const currentUrl = qs.parse(params)

  keysToRemove.forEach((key) => {
    delete currentUrl[key]
  })

  return qs.stringifyUrl(
    {
      url: window.location.pathname,
      query: currentUrl,
    },
    { skipNull: true }
  )
}

export const handleError = (error: unknown) => {
  console.error(error)

  let errorMessage: string

  if (typeof error === 'string') {
    errorMessage = error
  } else if (error instanceof Error) {
    errorMessage = error.message
  } else {
    try {
      errorMessage = JSON.stringify(error)
    } catch (stringifyError) {
      errorMessage = 'Неизвестная ошибка'
    }
  }

  throw new Error(errorMessage)
}
export const isEventFavorite = (eventId: string, favoriteEvents: string[]) => {
  return favoriteEvents.some((event) => event === eventId)
}

export const getUserFromCookie = () => {
  // const token = Cookies.get('token');
  // if (!token) return null;
  // try {
  //   const user = jwt.decode(token);
  //   return user;
  // } catch (error) {
  //   console.error('Error decoding token:', error);
  //   return null;
  // }
}
