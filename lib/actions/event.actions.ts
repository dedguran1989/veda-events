'use server'

import { revalidatePath } from 'next/cache'

import { connectToDatabase } from '@/lib/database'
import Event from '@/lib/database/models/event.model'
import User from '@/lib/database/models/user.model'
import Category from '@/lib/database/models/caregory.model'
import { handleError } from '@/lib/utils'

import {
  CreateEventParams,
  UpdateEventParams,
  DeleteEventParams,
  GetAllEventsParams,
  GetEventsByUserParams,
  GetRelatedEventsByCategoryParams,
} from '@/types'
import FavoriteEvent, {
  IFavoriteEvent,
} from '../database/models/favorite.model'
import { promises as fs } from 'fs'
import { join, extname } from 'path'

const getCategoryByName = async (name: string) => {
  return Category.findOne({ name: { $regex: name, $options: 'i' } })
}

const populateEvent = (query: any) => {
  return query
    .populate({
      path: 'organizer',
      model: User,
      select: '_id username role email',
    })
    .populate({ path: 'category', model: Category, select: '_id name' })
}

// CREATE
export async function createEvent({
  event,
  userEmail,
  path,
}: CreateEventParams) {
  try {
    await connectToDatabase()

    const organizer = await User.findOne({ email: userEmail })

    if (!organizer) throw new Error('Organizer not found')

    const newEvent = await Event.create({
      ...event,
      category: event.categoryId,
      organizer: organizer._id,
      isActive: organizer.role === 'admin' ? true : false,
    })
    revalidatePath(path)

    return JSON.parse(JSON.stringify(newEvent))
  } catch (error) {
    handleError(error)
  }
}

// GET ONE EVENT BY ID
export async function getEventById(eventId: string) {
  try {
    await connectToDatabase()

    const event = await populateEvent(
      Event.findOne({ _id: eventId, isActive: true })
    )

    if (!event) throw new Error('Event not found')

    return JSON.parse(JSON.stringify(event))
  } catch (error) {
    console.log(error)
    //handleError(error)
  }
}

// UPDATE
export async function updateEvent({
  userEmail,
  event,
  path,
}: UpdateEventParams) {
  try {
    await connectToDatabase()
    const organizer = await User.findOne({ email: userEmail })
    if (!organizer) {
      throw new Error('organizer or event not found')
    }
    const updatedEvent = await Event.findByIdAndUpdate(
      event._id,
      { ...event, category: event.categoryId },
      { new: true }
    )

    revalidatePath(path)

    return JSON.parse(JSON.stringify(updatedEvent))
  } catch (error) {
    handleError(error)
  }
}

// DELETE
export async function deleteEvent({ eventId, path }: DeleteEventParams) {
  try {
    await connectToDatabase()

    const deletedEvent = await Event.findByIdAndDelete(eventId)
    const uploadDir = join(process.cwd(), 'public', 'assets/uploads')
    console.log('deletedEvent', deletedEvent)
    if (deletedEvent) {
      await fs.unlink(`${uploadDir}/${deletedEvent.imageUrl}`)
      revalidatePath(path)
    }
  } catch (error) {
    handleError(error)
  }
}

// GET ALL EVENTS
export async function getAllEvents({
  query,
  limit = 6,
  page,
  category,
}: GetAllEventsParams) {
  try {
    await connectToDatabase()

    const titleCondition = query
      ? { title: { $regex: query, $options: 'i' } }
      : {}
    const categoryCondition = category
      ? await getCategoryByName(category)
      : null

    const conditions = {
      $and: [
        titleCondition,
        categoryCondition ? { category: categoryCondition._id } : {},
      ],
    }

    const skipAmount = (Number(page) - 1) * limit
    const eventsQuery = Event.find(conditions)
      .sort({ createdAt: 'desc' })
      .skip(skipAmount)
      .limit(limit)

    const events = await populateEvent(eventsQuery)
    const eventsCount = await Event.countDocuments(conditions)

    return {
      data: JSON.parse(JSON.stringify(events)),
      totalPages: Math.ceil(eventsCount / limit),
    }
  } catch (error) {
    handleError(error)
  }
}

// GET EVENTS BY ORGANIZER
export async function getEventsByUser({
  userEmail,
  limit = 6,
  page,
}: GetEventsByUserParams) {
  try {
    await connectToDatabase()
    const organizer = await User.findOne({ email: userEmail })
    const conditions = { organizer: organizer._id }
    const skipAmount = (page - 1) * limit

    const eventsQuery = Event.find(conditions)
      .sort({ createdAt: 'desc' })
      .skip(skipAmount)
      .limit(limit)

    const events = await populateEvent(eventsQuery)
    const eventsCount = await Event.countDocuments(conditions)

    return {
      data: JSON.parse(JSON.stringify(events)),
      totalPages: Math.ceil(eventsCount / limit),
    }
  } catch (error) {
    console.log('Ошибка в getEventsByUser', error)
    //handleError(error)
  }
}

// GET RELATED EVENTS: EVENTS WITH SAME CATEGORY
export async function getRelatedEventsByCategory({
  categoryId,
  eventId,
  limit = 3,
  page = 1,
}: GetRelatedEventsByCategoryParams) {
  try {
    await connectToDatabase()

    const skipAmount = (Number(page) - 1) * limit
    const conditions = {
      $and: [{ category: categoryId }, { _id: { $ne: eventId } }],
    }

    const eventsQuery = Event.find(conditions)
      .sort({ createdAt: 'desc' })
      .skip(skipAmount)
      .limit(limit)

    const events = await populateEvent(eventsQuery)
    const eventsCount = await Event.countDocuments(conditions)

    return {
      data: JSON.parse(JSON.stringify(events)),
      totalPages: Math.ceil(eventsCount / limit),
    }
  } catch (error) {
    handleError(error)
  }
}
// GET FAVORITE EVENTS BY USER
export async function getFavoriteEvents({
  userEmail,
  limit = 6,
  page = 1,
}: GetEventsByUserParams) {
  try {
    await connectToDatabase()

    const skipAmount = (page - 1) * limit
    const organizer = await User.findOne({ email: userEmail })
    if (!organizer) {
      throw new Error(`User not found for userEmail: ${userEmail}`)
    }
    // Step 1: Find favorite events for the user
    const favoriteEvents = await FavoriteEvent.find({
      userId: organizer._id,
    })
      .sort({ createdAt: 'desc' })
      .skip(skipAmount)
      .limit(limit)
      .exec()

    if (!favoriteEvents || favoriteEvents.length === 0) {
      return { data: [], totalPages: 0 }
    }

    // Extract event IDs
    const eventIds = favoriteEvents.map((fav: IFavoriteEvent) => fav.eventId)

    // Step 2: Find events details for the favorite event IDs
    const events = await Event.find({ _id: { $in: eventIds } })
      .populate({
        path: 'organizer',
        model: 'User',
        select: '_id username',
      })
      .populate({ path: 'category', model: 'Category', select: '_id name' })

    // Step 3: Get total count of favorite events for pagination
    const totalFavoritesCount = await FavoriteEvent.countDocuments({
      userId: organizer._id,
    })

    return {
      data: JSON.parse(JSON.stringify(events)),
      isFavoriteEvents: JSON.parse(JSON.stringify(eventIds)),
      totalPages: Math.ceil(totalFavoritesCount / limit),
    }
  } catch (error) {
    handleError(error)
  }
}
export async function toggleActiveForEvent(
  eventId: string,
  active: boolean,
  path: string
) {
  try {
    await connectToDatabase()
    const event = await Event.findById(eventId)
    if (!event) {
      throw new Error('Событие у которого нужно обновить активность не найдено')
    }
    event.isActive = active
    await event.save()
    revalidatePath(path)
    return { data: JSON.parse(JSON.stringify(event)) }
  } catch (error) {
    handleError(error)
  }
}
