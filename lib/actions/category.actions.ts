'use server'

import { CreateCategoryParams } from '@/types'
import { connectToDatabase } from '../database'
import Category from '../database/models/caregory.model'
import { handleError } from '../utils'

export const createCategory = async ({
  categoryName,
}: CreateCategoryParams) => {
  try {
    await connectToDatabase()

    const newCategory = await Category.create({ name: categoryName })
    return JSON.parse(JSON.stringify(newCategory))
  } catch (error) {
    handleError(error)
  }
}

export const getAllCategores = async () => {
  try {
    await connectToDatabase()

    const categores = await Category.find()
    return JSON.parse(JSON.stringify(categores))
  } catch (error) {
    handleError(error)
  }
}
