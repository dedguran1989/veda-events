'use server'

import { revalidatePath } from 'next/cache'
import bcrypt from 'bcryptjs'
import { connectToDatabase } from '@/lib/database'
import User from '@/lib/database/models/user.model'
import Event from '@/lib/database/models/event.model'
import { handleError } from '@/lib/utils'

import {
  CreateUserParams,
  UpdateUserParams,
  LoginUserParams,
  GetAllEventsParams,
  GetAllUsersParams,
} from '@/types'

const populateUsers = (query: any) => {
  return query
}

export const createUser = async (user: CreateUserParams) => {
  try {
    await connectToDatabase()
    const hashedPassword = await bcrypt.hash(user.password, 10)
    const newUser = await User.create({ ...user, password: hashedPassword })
    const userData = JSON.parse(JSON.stringify(newUser))

    return userData
  } catch (error) {
    console.error('Ошибка при создании пользователя:', error)
    throw new Error('Ошибка при создании пользователя')
  }
}
export async function getUserById(userId: string) {
  try {
    await connectToDatabase()

    const user = await User.findById(userId)

    if (!user) {
      return false
    }
    return JSON.parse(JSON.stringify(user))
  } catch (error) {
    handleError(error)
  }
}

export async function updateUser(clerkId: string, user: UpdateUserParams) {
  try {
    await connectToDatabase()

    const updatedUser = await User.findOneAndUpdate({ clerkId }, user, {
      new: true,
    })

    if (!updatedUser) throw new Error('User update failed')
    return JSON.parse(JSON.stringify(updatedUser))
  } catch (error) {
    handleError(error)
  }
}

export async function deleteUser(userId: string) {
  try {
    await connectToDatabase()

    // Find user to delete
    const userToDelete = await User.findById(userId)

    if (!userToDelete) {
      throw new Error('User not found')
    }

    // Unlink relationships
    await Promise.all([
      // Update the 'events' collection to remove references to the user
      Event.updateMany(
        { _id: { $in: userToDelete.events } },
        { $pull: { organizer: userToDelete._id } }
      ),

      // Update the 'orders' collection to remove references to the user
      // Delete articles associated with the user
      Event.deleteMany({ 'organizer._id': userId }),
    ])

    // Delete user
    const deletedUser = await User.findByIdAndDelete(userToDelete._id)
    revalidatePath('/')

    return deletedUser ? JSON.parse(JSON.stringify(deletedUser)) : null
  } catch (error) {
    handleError(error)
  }
}
export async function getUserByEmail(email: string) {
  try {
    await connectToDatabase()

    const user = await User.findOne({ email: email })

    if (!user) false
    return JSON.parse(JSON.stringify(user))
  } catch (error) {
    handleError(error)
  }
}

export async function loginUser(loginUser: LoginUserParams) {
  try {
    const { email, password } = loginUser

    await connectToDatabase()

    const user = await User.findOne({ email: email })
    const checkPassword = loginUser.bcrypt
      ? password === user.password
      : await bcrypt.compare(password, user.password)

    if (user && checkPassword) {
      return JSON.parse(JSON.stringify(user))
    } else {
      console.log('Пользователь не прошел проверку по паролю')
      return null
    }
  } catch (error) {
    handleError(error)
  }
}

export async function getAllUsers({
  query,
  limit = 6,
  page,
}: GetAllUsersParams) {
  try {
    await connectToDatabase()

    const titleCondition = query
      ? { title: { $regex: query, $options: 'i' } }
      : {}

    const conditions = {
      $and: [titleCondition],
    }

    const skipAmount = (Number(page) - 1) * limit
    const usersQuery = User.find(conditions)
      .sort({ createdAt: 'desc' })
      .skip(skipAmount)
      .limit(limit)

    const users = await populateUsers(usersQuery)
    const usersCount = await User.countDocuments(conditions)

    return {
      data: JSON.parse(JSON.stringify(users)),
      totalPages: Math.ceil(usersCount / limit),
    }
  } catch (error) {
    handleError(error)
  }
}
export async function toggleAdminForUser(
  userId: string,
  active: boolean,
  path: string
) {
  try {
    await connectToDatabase()
    const user = await User.findById(userId)
    if (!user) {
      throw new Error('Пользователь у которого нужно обновить роль не найден')
    }
    user.role = active === true ? 'admin' : 'user'
    await user.save()
    revalidatePath(path)
    return { data: JSON.parse(JSON.stringify(event)) }
  } catch (error) {
    handleError(error)
  }
}
