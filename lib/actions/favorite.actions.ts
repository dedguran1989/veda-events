'use server'

import { connectToDatabase } from '@/lib/database'
import { revalidatePath } from 'next/cache'

import { handleError } from '@/lib/utils'
import FavoriteEvent from '../database/models/favorite.model'
import Event from '@/lib/database/models/event.model'

export async function addFavorite(
  userId: string,
  eventId: string,
  path: string
) {
  try {
    await connectToDatabase()
    const favoriteEvent = await FavoriteEvent.create({ userId, eventId })
    if (favoriteEvent) {
      await Event.findByIdAndUpdate(eventId, { $inc: { favoriteCount: 1 } })
      revalidatePath(path)
    }

    return JSON.parse(JSON.stringify(favoriteEvent))
  } catch (error) {
    handleError(error)
  }
}

export async function removeFavorite(
  userId: string,
  eventId: string,
  path: string
) {
  try {
    await connectToDatabase()
    const result = await FavoriteEvent.deleteOne({ userId, eventId })

    if (result) {
      await Event.findByIdAndUpdate(eventId, { $inc: { favoriteCount: -1 } })
      revalidatePath(path)
    }
    return JSON.parse(JSON.stringify(result))
  } catch (error) {
    handleError(error)
  }
}
export async function getUserFavoriteEvents(userFavoriteObj: {
  userId: string
  page: number
  limit: number
}) {
  try {
    await connectToDatabase()

    const conditions = { userId: userFavoriteObj.userId }

    const favoriteEventsByUser = await FavoriteEvent.find(conditions).sort({
      createdAt: 'desc',
    })

    if (favoriteEventsByUser && favoriteEventsByUser.length > 0) {
      const favoriteEventIds = favoriteEventsByUser?.map(
        (favorite) => favorite.eventId
      )

      return JSON.parse(JSON.stringify(favoriteEventIds))
    } else {
      return []
    }
  } catch (error) {
    console.log('getUserFavoriteEvents', error)
  }
}
