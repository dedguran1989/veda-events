import { object, string, z } from 'zod'

export const eventFormSchema = z.object({
  title: z.string().min(3, {
    message: 'Заголовок должн иметь хотя бы 3 символа.',
  }),
  description: z
    .string()
    .min(3, {
      message: 'Описание должно иметь хотя бы 3 символа.',
    })
    .max(400, { message: 'Описание должно иметь максимум 400 символа.' }),
  location: z
    .string()
    .min(3, {
      message: 'Локация должна иметь хотя бы 3 символа.',
    })
    .max(400, { message: 'Локация должна иметь максимум 400 символа.' }),
  imageUrl: z.string(),
  startDateTime: z.date(),
  endDateTime: z.date(),
  categoryId: z.string(),
  price: z.string(),
  isFree: z.boolean(),
  url: z.string().url(),
})
export const signInSchema = object({
  email: string({ required_error: 'Электронная почта не подходит' })
    .min(1, 'Требуется электронная почта')
    .email('Неправильный email'),
  password: string({ required_error: 'Требуется пароль' })
    .min(1, 'Требуется пароль')
    .min(6, 'Пароль должен быть более 6 символов')
    .max(32, 'Пароль должен быть менее 32 символов'),
})
