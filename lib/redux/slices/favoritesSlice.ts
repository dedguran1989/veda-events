import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { addFavorite, removeFavorite } from '@/lib/actions/favorite.actions'
import { IFavoriteEvent } from '@/lib/database/models/favorite.model'
import store from '../store'

export interface FavoritesState {
  favorites: {
    items: IFavoriteEvent[]
    status: 'loading' | 'loaded' | 'error' | null
  }
}

export const addFavoriteAsync = createAsyncThunk(
  'favorites/addFavorite',
  async (
    {
      userId,
      eventId,
      path,
    }: { userId: string; eventId: string; path: string },
    thunkAPI
  ) => {
    try {
      const event = await addFavorite(userId, eventId, path)
      return event
    } catch (error) {
      console.warn('Ошибка при добавлении избранных событий в redux', error)
    }
  }
)

export const removeFavoriteAsync = createAsyncThunk(
  'favorites/removeFavorite',
  async (
    {
      userId,
      eventId,
      path,
    }: { userId: string; eventId: string; path: string },
    thunkAPI
  ) => {
    try {
      await removeFavorite(userId, eventId, path)
      return eventId
    } catch (error) {
      console.warn('Ошибка при удалении из избранных событий в redux', error)
    }
  }
)

const initialState: FavoritesState = {
  favorites: {
    items: [],
    status: 'loading',
  },
}

export const favoritesSlice = createSlice({
  name: 'favorites',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addFavoriteAsync.pending, (state) => {
        state.favorites.status = 'loading'
      })
      .addCase(addFavoriteAsync.fulfilled, (state, { payload }) => {
        state.favorites.status = 'loaded'
        state.favorites.items = payload
      })
      .addCase(addFavoriteAsync.rejected, (state) => {
        state.favorites.status = 'error'
      })
      .addCase(removeFavoriteAsync.pending, (state) => {
        state.favorites.status = 'loading'
      })
      .addCase(removeFavoriteAsync.fulfilled, (state, { payload }) => {
        state.favorites.status = 'loaded'
        state.favorites.items = state.favorites.items?.filter(
          (obj) => obj._id !== payload
        )
      })
      .addCase(removeFavoriteAsync.rejected, (state) => {
        state.favorites.status = 'error'
      })
  },
})

export const selectFavoriteEvents = (state: RootState) => state.favorites

// Экспортируем срез Redux
export default favoritesSlice.reducer

// Тип для корневого состояния Redux
type RootState = ReturnType<typeof store.getState>
