import { IEvent } from '@/lib/database/models/event.model'
import React, { useEffect, useState } from 'react'
import Card from './Card'
import Pagination from './Pagination'
import Link from 'next/link'
import { Button } from '../ui/button'

type CollectionProps = {
  data: IEvent[]
  emptyTitle: string
  emptyStateSubtext: string
  page: number | string
  limit: number
  totalPages?: number
  urlParamName?: string
  collectionType?: 'Events_Organized' | 'My_Favorites' | 'All_Events'
  favorites?: string[]
}

const Collection = ({
  data,
  emptyTitle,
  emptyStateSubtext,
  page,
  totalPages = 0,
  collectionType,
  urlParamName,
  limit,
  favorites,
}: CollectionProps) => {
  return (
    <>
      {data.length > 0 ? (
        <div className="flex flex-col items-center gap-10">
          <ul className="grid w-full grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:gap-10">
            {data.map((event) => {
              const hasOrderLink = collectionType === 'Events_Organized'
              const hideFavorite = collectionType === 'My_Favorites'
              const isFavorite = favorites?.includes(event._id)

              return (
                <>
                  {event.isActive ? (
                    <li key={event._id} className="flex justify-center">
                      <Card
                        event={event}
                        hideFavorite={hideFavorite}
                        hasOrderLink={hasOrderLink}
                        isFavorite={isFavorite}
                      />
                    </li>
                  ) : null}
                </>
              )
            })}
          </ul>
          {totalPages > 1 && (
            <Pagination
              urlParamName={urlParamName}
              page={page}
              totalPages={totalPages}
            />
          )}
        </div>
      ) : (
        <div className="flex-center wrapper min-h-[200px] w-full flex-col gap-3 rounded-[14px bg-grey-50 py-28 text-center]">
          <h3 className="p-bold-20 md:h5-bold">{emptyTitle}</h3>
          <Button size="lg" asChild className="button w-full sm:w-fit">
            <Link href="/events/create">Создайте событие</Link>
          </Button>
        </div>
      )}
    </>
  )
}

export default Collection
