'use client'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { Button } from '../ui/button'
import NavItems from './NavItems'
import MobileNav from './MobileNav'
import { useSession } from 'next-auth/react'
import { signIn, signOut } from 'next-auth/react'
import ExitButton from './ExitButton'

const Header = () => {
  const { data: session, status } = useSession()

  return (
    <header className="w-full border-b">
      <div className="wrapper flex items-center justify-between">
        <Link href="/">
          <Image
            src="/assets/images/logo.png"
            width={80}
            height={72}
            alt="Veda Event Logo"
          />
        </Link>
        {status === 'authenticated' ? (
          <>
            <nav className="md:flex-between hidden w-full max-w-xs">
              <NavItems />
            </nav>
            <div className="flex w-32 justify-end gap-3">
              <ExitButton
                photo={
                  session?.user?.image
                    ? session?.user?.image
                    : '/assets/images/user-logo.svg'
                }
                userRole={
                  session?.user?.email ===
                  process.env.NEXT_PUBLIC_ADMIN_EMAIL_ADDRESS
                    ? 'admin'
                    : 'user'
                }
              />
              <MobileNav />
            </div>
          </>
        ) : (
          <Button asChild className="rounded-full" size="lg">
            <div
              onClick={() => {
                signIn()
              }}
            >
              Войти
            </div>
          </Button>
        )}
        {/* <SignedIn>
          <nav className="md:flex-between hidden w-full max-w-xs">
            <NavItems />
          </nav>
        </SignedIn> */}
        {/* <div className="flex w-32 justify-end gap-3">
          <SignedIn>
            <UserButton afterSignOutUrl="/" />
            <MobileNav />
          </SignedIn>
          <SignedOut>
            <Button asChild className="rounded-full" size="lg">
              <Link href="/sign-in">Login</Link>
            </Button>
          </SignedOut>
        </div> */}
      </div>
    </header>
  )
}

export default Header
