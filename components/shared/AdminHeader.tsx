import Link from 'next/link'
import { Button } from '@/components/ui/button'

const AdminHeader = () => {
  return (
    <>
      <section className=" bg-primary-50 bg-dotted-pattern bg-cover bg-center py-5 md:py-10 flex align-middle justify-between">
        <h3 className="wrapper h3-bold text-center sm:text-left ">
          Админ панель
        </h3>
      </section>

      <section className="wrapper mt-8">
        <Button size="lg" asChild className="button w-full sm:w-fit">
          <Link href="/">Главная страница</Link>
        </Button>
        <Button size="lg" asChild className="button w-full sm:w-fit ml-10">
          <Link href="/admin/users">События</Link>
        </Button>
        <Button size="lg" asChild className="button w-full sm:w-fit ml-10">
          <Link href="/admin/users">Пользователи</Link>
        </Button>
      </section>
    </>
  )
}

export default AdminHeader
