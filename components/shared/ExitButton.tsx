import {
  Menubar,
  MenubarContent,
  MenubarItem,
  MenubarMenu,
  MenubarSeparator,
  MenubarShortcut,
  MenubarTrigger,
} from '@/components/ui/menubar'
import Image from 'next/image'
import { signOut } from 'next-auth/react'
import Link from 'next/link'
import { getUserByEmail } from '@/lib/actions/user.actions'
import { useSession } from 'next-auth/react'
type ExitButtonProps = {
  photo: string
  userRole: 'admin' | 'user'
}

export default function ExitButton({ photo, userRole }: ExitButtonProps) {
  return (
    <>
      <Menubar className="border-0">
        <MenubarMenu>
          <MenubarTrigger className="rounded-full w-10 h-10 p-0 border-0">
            {' '}
            <Image
              src={photo}
              width={38}
              height={38}
              alt="logo"
              className="rounded-full"
            />
          </MenubarTrigger>
          <MenubarContent>
            {userRole === 'admin' ? (
              <MenubarItem className="flex justify-center">
                <Link href="/admin/events">Админ панель</Link>
              </MenubarItem>
            ) : null}

            <MenubarSeparator />
            <MenubarItem
              onClick={() => {
                signOut()
              }}
              className="rounded-full bg-primary-500 text-white text-center w-full h-10 flex justify-center"
            >
              Выйти
            </MenubarItem>
          </MenubarContent>
        </MenubarMenu>
      </Menubar>
    </>
  )
}
