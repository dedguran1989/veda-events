'use client'

import { Button } from '@/components/ui/button'
import { IEvent } from '@/lib/database/models/event.model'
import { formatDateTime } from '@/lib/utils'
import Link from 'next/link'
import Image from 'next/image'
import { usePathname } from 'next/navigation'
import { DeleteConfirmation } from '@/components/shared/DeleteConfirmation'
import { toggleActiveForEvent } from '@/lib/actions/event.actions'

interface EventTableProps {
  events: IEvent[]
}

const EventTable: React.FC<EventTableProps> = ({ events }) => {
  const pathname = usePathname()
  const handleToggleActive = (id: string, active: boolean) => async () => {
    const changeEvent = await toggleActiveForEvent(id, active, pathname)
    console.log('changeEvent', changeEvent)
  }
  return (
    <table className="w-full border-collapse border-t">
      <thead>
        <tr className="p-medium-14 border-b text-grey-500">
          <th className="min-w-[200px] flex-1 py-3 pr-4 text-left">
            Заголовок
          </th>
          <th className="min-w-[150px] py-3 text-left">Описание</th>
          <th className="min-w-[150px] py-3 text-left">Автор</th>
          <th className="min-w-[100px] py-3 text-left">Кол-во в избранное</th>
          <th className="min-w-[100px] py-3 text-left">Активность</th>
          <th className="min-w-[100px] py-3 text-left">Категория</th>
          <th className="min-w-[100px] py-3 text-left">Дата Создания</th>
          <th className="min-w-[100px] py-3 text-left">
            Редактирование/Удаление
          </th>
        </tr>
      </thead>
      <tbody>
        {events.length === 0 ? (
          <tr className="border-b">
            <td colSpan={8} className="py-4 text-center text-gray-500">
              События не найдены.
            </td>
          </tr>
        ) : (
          events.map((row: IEvent) => (
            <tr key={row._id} className="p-regular-14 lg:p-regular-16 border-b">
              <td className="min-w-[200px] flex-1 py-4 pr-4">{row.title}</td>
              <td className="min-w-[150px] py-4">{row.description}</td>
              <td className="min-w-[150px] py-4">{row.organizer.username}</td>
              <td className="min-w-[150px] py-4">{row.favoriteCount}</td>
              <td className="min-w-[150px] py-4">
                {row.isActive ? (
                  <Button onClick={handleToggleActive(row._id, false)}>
                    Активная
                  </Button>
                ) : (
                  <Button onClick={handleToggleActive(row._id, true)}>
                    Неактивная
                  </Button>
                )}
              </td>
              <td className="min-w-[150px] py-4">{row.category.name}</td>
              <td className="min-w-[100px] py-4">
                {formatDateTime(row.createdAt).dateTime}
              </td>
              <td className="min-w-[100px] py-4">
                <div className="flex justify-center">
                  <Link href={`/events/${row._id}/update`}>
                    <Image
                      src="/assets/icons/edit.svg"
                      alt="edit"
                      width={20}
                      height={20}
                    />
                  </Link>
                  <DeleteConfirmation eventId={row._id} />
                </div>
              </td>
            </tr>
          ))
        )}
      </tbody>
    </table>
  )
}

export default EventTable
