'use client'

import { formatDateTime } from '@/lib/utils'
import { usePathname } from 'next/navigation'
import { toggleActiveForEvent } from '@/lib/actions/event.actions'
import { IUser } from '@/lib/database/models/user.model'
import { DeleteUserConfirmation } from './DeleteUserConfirmation'
import { Button } from '../ui/button'
import { toggleAdminForUser } from '@/lib/actions/user.actions'

interface UsersTableProps {
  users: IUser[]
}

const UsersTable: React.FC<UsersTableProps> = ({ users }) => {
  const pathname = usePathname()
  const handleToggleActive = (id: string, active: boolean) => async () => {
    const changeUserRole = await toggleAdminForUser(id, active, pathname)
  }
  return (
    <table className="w-full border-collapse border-t">
      <thead>
        <tr className="p-medium-14 border-b text-grey-500">
          <th className="min-w-[200px] flex-1 py-3 pr-4 text-left">
            Заголовок
          </th>
          <th className="min-w-[150px] py-3 text-left">Email</th>
          <th className="min-w-[150px] py-3 text-left">Имя</th>
          <th className="min-w-[100px] py-3 text-left">Роль</th>
          <th className="min-w-[100px] py-3 text-left">Дата Создания</th>
          <th className="min-w-[100px] py-3 text-left">Удаление</th>
        </tr>
      </thead>
      <tbody>
        {users.length === 0 ? (
          <tr className="border-b">
            <td colSpan={8} className="py-4 text-center text-gray-500">
              Пользователи не найдены.
            </td>
          </tr>
        ) : (
          users.map((row: IUser) => (
            <tr key={row._id} className="p-regular-14 lg:p-regular-16 border-b">
              <td className="min-w-[200px] flex-1 py-4 pr-4">Пользователь</td>
              <td className="min-w-[200px] flex-1 py-4 pr-4">{row.email}</td>
              <td className="min-w-[150px] py-4">{row.username}</td>
              <td className="min-w-[150px] py-4">
                {row.role === 'admin' ? (
                  <Button onClick={handleToggleActive(row._id, false)}>
                    Админ
                  </Button>
                ) : (
                  <Button onClick={handleToggleActive(row._id, false)}>
                    Обычный пользователь
                  </Button>
                )}
              </td>
              <td className="min-w-[150px] py-4">
                {formatDateTime(row.createdAt).dateTime}
              </td>

              <td className="min-w-[100px] py-4">
                <div className="flex justify-center">
                  <DeleteUserConfirmation userId={row._id} />
                </div>
              </td>
            </tr>
          ))
        )}
      </tbody>
    </table>
  )
}

export default UsersTable
