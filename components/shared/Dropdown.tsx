import React, { startTransition, useEffect, useState } from 'react'
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select'
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from '@/components/ui/alert-dialog'

import { ICategory } from '@/lib/database/models/caregory.model'
import { Input } from '../ui/input'
import { createCategory, getAllCategores } from '@/lib/actions/category.actions'
import { useSession } from 'next-auth/react'

type DropdownProps = {
  value?: string
  onChangeHandler?: () => void
}
const Dropdown = ({ value, onChangeHandler }: DropdownProps) => {
  const { data: session, status } = useSession()
  const isAdmin =
    session?.user?.email === process.env.NEXT_PUBLIC_ADMIN_EMAIL_ADDRESS
  const [categories, setCategories] = useState<ICategory[]>([])
  const [newCategory, setNewCategory] = useState('')

  const handleAddCategory = () => {
    createCategory({
      categoryName: newCategory.trim(),
    }).then((category) => {
      setCategories((prevState) => [...prevState, category])
    })
  }
  useEffect(() => {
    const getCategories = async () => {
      const categoryList = await getAllCategores()
      categoryList && setCategories(categoryList as ICategory[])
    }
    getCategories()
  }, [])
  console.log(
    'session',
    session?.user?.email,
    process.env.NEXT_PUBLIC_ADMIN_EMAIL_ADDRESS
  )
  return (
    <Select onValueChange={onChangeHandler} value={value}>
      <SelectTrigger className="select-field">
        <SelectValue placeholder="Категории" />
      </SelectTrigger>
      <SelectContent>
        {categories.length > 0 &&
          categories.map((category) => (
            <SelectItem
              key={category._id}
              value={category._id}
              className="select-item p-regular-14"
            >
              {category.name}
            </SelectItem>
          ))}
        <AlertDialog>
          {isAdmin ? (
            <AlertDialogTrigger className="p-medium-14 flex w-full rounded-sm py-3 pl-8 text-primary-500 hover:bg-primary-50 focus:text-primary-500 ">
              Добавьте новую категорию
            </AlertDialogTrigger>
          ) : null}

          <AlertDialogContent className="bg-white">
            <AlertDialogHeader>
              <AlertDialogTitle>Новая Категория</AlertDialogTitle>
              <AlertDialogDescription>
                <Input
                  type="text"
                  placeholder="Введите название категории"
                  className="input-field mt-3"
                  onChange={(e) => {
                    setNewCategory(e.target.value)
                  }}
                />
              </AlertDialogDescription>
            </AlertDialogHeader>
            <AlertDialogFooter>
              <AlertDialogCancel>Отмена</AlertDialogCancel>
              <AlertDialogAction
                onClick={() => startTransition(handleAddCategory)}
              >
                Добавить
              </AlertDialogAction>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialog>
      </SelectContent>
    </Select>
  )
}

export default Dropdown
