'use client'
import { addFavorite, removeFavorite } from '@/lib/actions/favorite.actions'
import { handleError } from '@/lib/utils'
import { usePathname } from 'next/navigation'
import Image from 'next/image'
import React from 'react'
type FavoriteButtonProps = {
  userId: string
  eventId: string
  isFavorite: boolean | undefined
}
const FavoriteButton = ({
  userId,
  eventId,
  isFavorite,
}: FavoriteButtonProps) => {
  const pathname = usePathname()
  const addInFavorite = async () => {
    try {
      await addFavorite(userId, eventId, pathname)
    } catch (error) {
      handleError(error)
    }
  }
  const removeInFavorite = async () => {
    await removeFavorite(userId, eventId, pathname)
  }

  return (
    <>
      {!isFavorite ? (
        <div
          className="cursor-pointer p-medium-16 rounded-full "
          onClick={addInFavorite}
        >
          <Image
            src="/assets/icons/favorite.png"
            alt="Добавить в избранное"
            width={50}
            height={50}
          />
        </div>
      ) : (
        <div
          className="cursor-pointer p-medium-16 rounded-full "
          onClick={removeInFavorite}
        >
          <Image
            src="/assets/icons/favoriteActive.png"
            alt="Добавить в избранное"
            width={50}
            height={50}
          />
        </div>
      )}
    </>
  )
}

export default FavoriteButton
