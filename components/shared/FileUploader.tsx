'use client'
import { useEffect } from 'react'
import { useCallback, Dispatch, SetStateAction, useState } from 'react'
import { useDropzone } from 'react-dropzone'

import { Button } from '@/components/ui/button'
import { convertFileToUrl } from '@/lib/utils'

type FileUploaderProps = {
  onFieldChange: (url: string) => void
  imageUrl: string
  setFiles: Dispatch<SetStateAction<File[]>>
}

export function FileUploader({
  imageUrl,
  onFieldChange,
  setFiles,
}: FileUploaderProps) {
  const [pageType, setPageType] = useState<'Update' | 'Create'>('Create')

  useEffect(() => {
    const url = window.location.href
    if (url.includes('update')) {
      setPageType('Update')
    } else if (url.includes('create')) {
      setPageType('Create')
    }
  }, [])
  const onDrop = useCallback(
    async (acceptedFiles: any[]) => {
      setFiles(acceptedFiles)
      onFieldChange(convertFileToUrl(acceptedFiles[0]))
    },
    [setFiles, onFieldChange]
  )

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: {
      'image/jpeg': ['.jpeg', '.jpg'],
      'image/png': ['.png'],
      'image/svg+xml': ['.svg'],
    },
    maxSize: 25 * 1024 * 1024,
  })

  return (
    <div
      {...getRootProps()}
      className="flex-center bg-dark-3 flex h-72 cursor-pointer flex-col overflow-hidden rounded-xl bg-grey-50"
    >
      <input {...getInputProps()} className="cursor-pointer" />

      {imageUrl ? (
        <div className="flex h-full w-full flex-1 justify-center ">
          <img
            src={
              imageUrl.includes('http')
                ? imageUrl
                : `/assets/uploads/${imageUrl}`
            }
            alt="image"
            width={250}
            height={250}
            className="w-full object-cover object-center"
          />
        </div>
      ) : (
        <div className="flex-center flex-col py-5 text-grey-500">
          <img
            src="/assets/icons/upload.svg"
            width={77}
            height={77}
            alt="file upload"
          />
          <h3 className="mb-2 mt-2">Вставь фото сюда не более 25 МБ </h3>
          <p className="p-medium-12 mb-4">SVG, PNG, JPG</p>
          <Button type="button" className="rounded-full">
            Выбери с компьютера
          </Button>
        </div>
      )}
    </div>
  )
}
