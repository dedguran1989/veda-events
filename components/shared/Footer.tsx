import Link from 'next/link'
import Image from 'next/image'

const Footer = () => {
  return (
    <footer className="border-t ">
      <div className="flex-center wrapper flex-between flex flex-col gap-4 p-5 text-center sm:flex-row">
        <Link href="/">
          <Image
            src="/assets/images/logo.png"
            alt="logo"
            width={80}
            height={72}
          />
        </Link>
        <p>2024 Все права защищены</p>
      </div>
    </footer>
  )
}

export default Footer
