import NextAuth from 'next-auth'
import Google from 'next-auth/providers/google'
import GitHub from 'next-auth/providers/github'
import Credentials from 'next-auth/providers/credentials'
import { signInSchema } from './lib/validator'
import {
  createUser,
  getUserByEmail,
  getUserById,
  loginUser,
} from './lib/actions/user.actions'
import jwt from 'jsonwebtoken'
const secret = process.env.JWT_SECRET || 'your_jwt_secret'

export const { handlers, signIn, signOut, auth } = NextAuth({
  providers: [
    Google,
    GitHub,
    Credentials({
      name: 'Credentials',
      id: 'credentials',
      credentials: {
        email: { label: 'Email', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      authorize: async (credentials) => {
        if (!credentials) {
          throw new Error('No credentials.')
        }

        try {
          const { email, password } = await signInSchema.parseAsync(credentials)
          const user = await loginUser({ email, password, bcrypt: false })
          if (user) {
            return user
          } else {
            const objRegistration = {
              username: email,
              email: email,
              password: password,
              photo: '',
              role: email == process.env.ADMIN_EMAIL_ADDRESS ? 'admin' : 'user',
            }

            const registerUser = await createUser(objRegistration)

            if (registerUser) {
              return registerUser
            }
          }

          console.log(
            'Не удалось залогинить или создать пользователя в Credentials'
          )
        } catch (error) {
          console.log(
            'Не получилось зарегестрировать пользователя после авторизации',
            error
          )
        }
      },
    }),
  ],
  callbacks: {
    signIn: async ({ user, account, profile, email, credentials }) => {
      console.log('SignIn Callback - User:', user)
      try {
        const userEmail = user.email

        if (!userEmail) {
          throw new Error('userEmail is required for sign in')
        }
        const checkUser = await getUserByEmail(userEmail)

        if (!checkUser) {
          const regObj = {
            username:
              account?.provider === 'google' || account?.provider === 'github'
                ? user.name
                : user.email,
            email: user.email,
            password:
              account?.provider === 'google' || account?.provider === 'github'
                ? user.email
                : user.name,
            photo: user.image,
            role:
              user.email == process.env.ADMIN_EMAIL_ADDRESS ? 'admin' : 'user',
          }
          const registerUser = await createUser({
            username: regObj.username || '',
            email: regObj.email || '',
            password: regObj.password || '',
            photo: regObj.photo || '',
            role: regObj.role,
          })

          if (registerUser) {
            return registerUser
          } else {
            return null
          }
        } else {
          const logObj = {
            email: checkUser.email,
            password: checkUser.password,
            bcrypt: true,
          }
          const loginUserData = await loginUser(logObj)

          if (loginUserData) {
            return loginUserData
          } else {
            return null
          }
        }
      } catch (err) {
        console.log('Не получилось войти с signIn', err)
      }
      return true
    },
    async jwt({ token, user }) {
      return token
    },
    async session({ session, token }) {
      return session
    },
  },
  secret,
  session: {
    strategy: 'jwt',
  },
})
