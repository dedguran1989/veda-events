import Search from '@/components/shared/Search'
import { getAllEvents, toggleActiveForEvent } from '@/lib/actions/event.actions'

import { SearchParamProps } from '@/types'
import EventTable from '@/components/shared/EventTable'
import AdminHeader from '@/components/shared/AdminHeader'

const Admin = async ({ searchParams }: SearchParamProps) => {
  const eventId = (searchParams?.eventId as string) || ''
  const searchText = (searchParams?.query as string) || ''
  const category = (searchParams?.category as string) || ''
  const page = Number(searchParams?.page || 1)

  const events = await getAllEvents({
    query: searchText,
    category,
    page,
    limit: 6,
  })

  return (
    <>
      <AdminHeader />
      <section className="wrapper mt-8">
        <Search placeholder="Поиск события..." />
      </section>

      <section className="wrapper overflow-x-auto">
        <EventTable events={events?.data} />
      </section>
    </>
  )
}

export default Admin
