import Search from '@/components/shared/Search'
import { getAllEvents, toggleActiveForEvent } from '@/lib/actions/event.actions'

import { SearchParamProps } from '@/types'
import UsersTable from '@/components/shared/UsersTable'
import AdminHeader from '@/components/shared/AdminHeader'
import { getAllUsers } from '@/lib/actions/user.actions'

const Users = async ({ searchParams }: SearchParamProps) => {
  const eventId = (searchParams?.eventId as string) || ''
  const searchText = (searchParams?.query as string) || ''
  const category = (searchParams?.category as string) || ''
  const page = Number(searchParams?.page || 1)

  const users = await getAllUsers({
    query: searchText,
    page,
    limit: 6,
  })

  return (
    <>
      <AdminHeader />
      <section className="wrapper mt-8">
        <Search placeholder="Поиск пользователя..." />
      </section>

      <section className="wrapper overflow-x-auto">
        <UsersTable users={users?.data} />
      </section>
    </>
  )
}

export default Users
