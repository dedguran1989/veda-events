import type { Metadata } from 'next'
import { Poppins } from 'next/font/google'

import './globals.css'
import SessionWrapper from '@/components/shared/SessionWrapper'

const poppins = Poppins({
  subsets: ['latin'],
  weight: ['400', '500', '600', '700'],
  variable: '--font-poppins',
})

export const metadata: Metadata = {
  title: 'Veda Event',
  description: 'Veda Event Website',
  icons: {
    icon: '/assets/images/logo.svg',
  },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <SessionWrapper>
      <html lang="en">
        <body className={poppins.variable}>{children}</body>
      </html>
    </SessionWrapper>
  )
}
