import { unlink, writeFile } from 'fs/promises'
import { NextRequest, NextResponse } from 'next/server'
import { join, extname } from 'path'
import { v4 as uuidv4 } from 'uuid'
import { promises as fs } from 'fs'

export async function POST(request: NextRequest) {
  const data = await request.formData()
  const file: File | null = data.get('file') as unknown as File
  const uploadImage: string | null = data.get(
    'uploadImage'
  ) as unknown as string
  console.log('oldUrl#', data)
  if (!file) {
    return NextResponse.json({ success: false })
  }

  const bytes = await file.arrayBuffer()
  const buffer = Buffer.from(bytes)
  const fileExtension = extname(file.name)

  const uniqueFileName = `${uuidv4()}${fileExtension}`
  const uploadDir = join(process.cwd(), 'public', 'assets/uploads')

  const path = join(uploadDir, uniqueFileName)
  if (uploadImage) {
    const oldImagePath = join(uploadDir, uploadImage)
    console.log('uploadImage', oldImagePath)
    try {
      await fs.unlink(oldImagePath)
    } catch (error) {
      console.error('Failed to delete old image:', error)
      // Optionally handle this error
    }
  }
  await writeFile(path, buffer)

  return NextResponse.json({ success: true, url: uniqueFileName })
}
