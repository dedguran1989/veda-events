import CategoryFilter from '@/components/shared/CategoryFilter'
import Collection from '@/components/shared/Collection'
import Search from '@/components/shared/Search'
import { Button } from '@/components/ui/button'
import { getAllEvents, getFavoriteEvents } from '@/lib/actions/event.actions'

import { auth } from '../../auth'

import { SearchParamProps, MainEvents } from '@/types'

import Image from 'next/image'
import Link from 'next/link'

export default async function Home({ searchParams }: SearchParamProps) {
  const session = await auth()

  const userEmail = session?.user?.email as string
  const page = Number(searchParams?.page || 1)
  const searchText = (searchParams?.query as string) || ''
  const category = (searchParams?.category as string) || ''

  const events = await getAllEvents({
    query: searchText,
    category,
    page,
    limit: 6,
  })

  const favoriteUserEvents = userEmail
    ? await getFavoriteEvents({ userEmail, page, limit: 6 })
    : { data: [], totalPages: 0 }

  return (
    <>
      <section className="bg-primary-50 bg-dotted-pattern bg-contain py-5 md:py-10">
        <div className="wrapper grid grid-cols-1 gap-5 md:grid-cols-2 2xl:gap-0">
          <div className="flex flex-col justify-center gap-8">
            <h1 className="h1-bold">
              Расскажи о событиях, которые помогут улучшить этот мир!
            </h1>
            <p className="p-regular-20 md:p-regular-24">
              Сообщи о мероприятии, которое позволят сделать этот мир лучше.
            </p>
            <Button size="lg" asChild className="button w-full sm:w-fit">
              <Link href="#events">Попробуйте Сейчас</Link>
            </Button>
          </div>

          <Image
            src="/assets/images/hero.png"
            alt="hero"
            width={1000}
            height={1000}
            className="max-h-[70vh] object-contain object-center 2xl:max-h-[50vh]"
          />
        </div>
      </section>
      <section
        id="events"
        className="wrapper my-8 flex flex-col gap-8 md:gap-12"
      >
        <h2 className="h2-bold">
          Найди <br /> то что по душе
        </h2>

        <div className="flex w-full flex-col gap-5 md:flex-row">
          <Search />
          <CategoryFilter />
        </div>

        <Collection
          data={events?.data}
          emptyTitle="События не найдены"
          emptyStateSubtext="Возвращайтесь позже"
          collectionType="All_Events"
          limit={6}
          page={page}
          totalPages={events?.totalPages}
          favorites={favoriteUserEvents?.isFavoriteEvents}
        />
      </section>
    </>
  )
}
