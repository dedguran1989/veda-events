import Footer from '@/components/shared/Footer'
import Header from '@/components/shared/Header'
import SessionWrapper from '@/components/shared/SessionWrapper'

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <SessionWrapper>
      <div className="flex h-screen flex-col">
        <Header />
        <main className="flex-1">{children}</main>
        <Footer />
      </div>
    </SessionWrapper>
  )
}
