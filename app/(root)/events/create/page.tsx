import { auth } from '@/auth'
import EventForm from '@/components/shared/EventForm'

const CreateEvent = async () => {
  const session = await auth()

  const userEmail = session?.user?.email as string

  return (
    <>
      <section className="bg-primary-50 bg-dotted-pattern bg-cover bg-center py-5 md:py-10">
        <h3 className="wrapper h3-bold text-center sm:text-left">
          Создай Событие
        </h3>
      </section>
      <div className="wrapper my-8">
        <EventForm userEmail={userEmail} type="Create" />
      </div>
    </>
  )
}

export default CreateEvent
