import Collection from '@/components/shared/Collection'
import { Button } from '@/components/ui/button'
import { getEventsByUser, getFavoriteEvents } from '@/lib/actions/event.actions'
import { auth } from '../../../auth'

import { SearchParamProps } from '@/types'

import Link from 'next/link'
import React from 'react'

const ProfilePage = async ({ searchParams }: SearchParamProps) => {
  const session = await auth()
  const userEmail = session?.user?.email as string

  const ordersPage = Number(searchParams?.ordersPage) || 1
  const eventsPage = Number(searchParams?.eventsPage) || 1

  const favoriteEvents = await getFavoriteEvents({ userEmail, page: 1 })
  //const favoriteEvents = await getUserFavoriteEvents(userId)
  const organizedEvents = await getEventsByUser({ userEmail, page: 1 })

  return (
    <>
      {/*My Tickets */}
      <section className="bg-primary-50 bg-dotted-pattern bg-cover bg-center py-5 md:py-10">
        <div className="wrapper flex items-center justify-center sm:justify-between">
          <h3 className="h3-bold text-center sm:text-left">
            Мои избранные события
          </h3>
        </div>
      </section>

      <section className="wrapper my-8">
        <Collection
          data={favoriteEvents?.data}
          emptyTitle="Нет опубликованных событий"
          emptyStateSubtext="Не переживайте вы можете опубликовать новое событие!"
          collectionType="My_Favorites"
          limit={3}
          page={1}
          urlParamName="ordersPage"
          totalPages={1}
          favorites={favoriteEvents?.isFavoriteEvents}
        />
      </section>

      {/* Events Organized */}
      <section className="bg-primary-50 bg-dotted-pattern bg-cover bg-center py-5 md:py-10">
        <div className="wrapper flex items-center justify-center sm:justify-between">
          <h3 className="h3-bold text-center sm:text-left">
            Опубликованные события
          </h3>
          <Button asChild size="lg" className="button hidden sm:flex">
            <Link href="/events/create">Создай новое событие</Link>
          </Button>
        </div>
      </section>

      <section className="wrapper my-8">
        <Collection
          data={organizedEvents?.data}
          emptyTitle="Мероприятия еще не созданы"
          emptyStateSubtext="Создай что нибудь новое"
          collectionType="Events_Organized"
          limit={3}
          page={eventsPage}
          urlParamName="eventsPage"
          totalPages={organizedEvents?.totalPages}
          favorites={favoriteEvents?.isFavoriteEvents}
        />
      </section>
    </>
  )
}

export default ProfilePage
