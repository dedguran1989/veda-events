export const headerLinks = [
  {
    label: 'Главная',
    route: '/',
  },
  {
    label: 'Создать Событие',
    route: '/events/create',
  },
  {
    label: 'Мой Профиль',
    route: '/profile',
  },
]

export const eventDefaultValues = {
  title: '',
  description: '',
  location: '',
  imageUrl: '',
  startDateTime: new Date(),
  endDateTime: new Date(),
  categoryId: '',
  price: '',
  isFree: false,
  url: '',
}
